def Articles():
    articles = [
        {
            'id': 1,
            'title': 'Article One',
            'body': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.\
Debitis neque iusto optio! Rem non nam sequi nemo accusamus a quam, vitae\
deserunt reprehenderit ullam eveniet tenetur ipsam, earum voluptatum\
doloremque!',
            'author': 'Edgar Ramírez',
            'create_date': '04-28-2018'
        },
        {
            'id': 2,
            'title': 'Article Two',
            'body': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.\
Debitis neque iusto optio! Rem non nam sequi nemo accusamus a quam, vitae\
deserunt reprehenderit ullam eveniet tenetur ipsam, earum voluptatum\
doloremque!',
            'author': 'Juan Pérez',
            'create_date': '04-28-2018'
        },
        {
            'id': 3,
            'title': 'Article Three',
            'body': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.\
Debitis neque iusto optio! Rem non nam sequi nemo accusamus a quam, vitae\
deserunt reprehenderit ullam eveniet tenetur ipsam, earum voluptatum\
doloremque!',
            'author': 'El Santo',
            'create_date': '04-28-2018'
        }
    ]

    return articles
