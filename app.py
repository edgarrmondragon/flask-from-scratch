from flask import (Flask, render_template, flash, redirect, url_for,
                   session, logging, request)
from data import Articles
from flask_mysqldb import MySQL
from wtforms import (Form, BooleanField, StringField, TextAreaField,
                     PasswordField, validators)
from wtforms.fields.html5 import EmailField
from flask_wtf.csrf import CSRFProtect
from passlib.hash import sha256_crypt

app = Flask(__name__)
app.config['SECRET_KEY'] = '12345678'
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '12358wyvern*'
app.config['MYSQL_DB'] = 'myflaskapp'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)
csrf = CSRFProtect(app)

Articles = Articles()


@app.route('/')
def index():
    return render_template('home.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/articles')
def articles():
    return render_template('articles.html', articles=Articles)


@app.route('/articles/<int:id>/')
def article(id):
    return render_template('article.html', id=id)


class RegisterForm(Form):
    """docstring for RegisterForm"""
    name = StringField('Name', [
        validators.DataRequired(),
        validators.Length(min=4, max=50)
        ])
    username = StringField('Username', [
        validators.DataRequired(),
        validators.Length(min=4, max=25)
        ])
    email = EmailField('Email', [
        validators.DataRequired(),
        validators.Length(min=6, max=50),
        validators.Email("This field requires a valid email address")
        ])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message="Passwords do not match")
        ])
    confirm = PasswordField('Confirm Password', [
        validators.DataRequired()
        ])
    accept_tos = BooleanField('I accept the TOS', [
        validators.DataRequired()
        ])


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = sha256_crypt.encrypt(str(form.password.data))

        # Create cursor
        cur = mysql.connection.cursor()

        cur.execute('INSERT INTO users(name, email, username, password)\
VALUES(%s, %s, %s, %s);', (name, email, username, password))

        # Commit to DB
        mysql.connection.commit()

        # Close connection
        cur.close()

        flash('You are now registered and can log in', 'success')

        redirect(url_for('index'))

        return render_template('register.html', form=form)

    return render_template('register.html', form=form)

if __name__ == '__main__':
    app.run(debug=True)
